// Jacob Hilker
var camera;
var scene;
var renderer;
const objects = [];

function init() {
  // Initializes a scene.
  camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  scene = new THREE.Scene();
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x1c253c, 1)
  document.body.appendChild(renderer.domElement);
}

function drawPlanet() {
  // Draws the icy planet in the center of the screen
  var planetGeo = new THREE.SphereGeometry(radius=2.3, widthSegments=32,heightSegments=32); 
  var planetMat = new THREE.MeshBasicMaterial({color: 0xb9ffef});

  var planet = new THREE.Mesh(planetGeo, planetMat);
  planet.scale.set(1.25,1.25,1.25);
  scene.add(planet);
  objects.push(planet);
  planet.position.y -= 0.75;
}

function drawSat() {
  // draws the satellite orbiting around the planet.
  var satGeo = new THREE.BoxGeometry();
  var satMat = new THREE.MeshBasicMaterial({color: 0x505050});
  var sat = new THREE.Mesh(satGeo, satMat);
   
  sat.scale.set(1, 0.5, 0.5);
  //sat.position.z += 2;
  scene.add(sat);
  objects.push(sat);
  
  sat.rotation.z += 0.5;
  sat.position.z += 2.5;
  sat.position.y -= 0.3;  
  //drawPanel();
}

function drawMoon() {
  // Draws the moon orbiting the planet.
  var moonGeo = new THREE.SphereGeometry(radius=1, widthSegments=32,heightSegments=32); 
  var moonMat = new THREE.MeshBasicMaterial({color: 0x777777});

  var moon = new THREE.Mesh(moonGeo, moonMat);
  //europa.scale.set(1.5,1.5,1.5);
  scene.add(moon);
  objects.push(moon);
  //moon.position.z += 1;
  
  moon.scale.set(0.65, 0.65, 0.65);
}

function drawSun() {
  // Draws the star off in the distance in the top left corner.
  var sunGeo = new THREE.SphereGeometry(radius=1.5, widthSegments=32,heightSegments=32); 
  var sunMat = new THREE.MeshBasicMaterial({color: 0xF28C38 });

  var sun = new THREE.Mesh(sunGeo, sunMat);
  scene.add(sun);
  objects.push(sun);
  sun.position.z -= 13;
  sun.position.y += 13;
  sun.position.x -= 13;
  sun.scale.set(0.15,0.15,0.15);
}

init();
drawPlanet();
drawSat();
drawMoon();
drawSun();
camera.position.z = 9;


const animate = function () {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
  objects[0].rotation.y += 0.01;
  objects[2].rotation.y += 0.01;

  // code originally taken from this thread - https://stackoverflow.com/questions/42418958/rotate-an-object-around-an-orbit
  // specifically this answer - https://stackoverflow.com/a/42419603/13192204
  // orbits the satellite and moon around the planet
  var satOrbitRadius = 3.5;
  var moonOrbitRadius = 4;
  var satSpeed = Date.now() * 0.002;
  var date = Date.now() * 0.001;
  objects[1].position.set(
    Math.cos(satSpeed) * satOrbitRadius * -1, 
    -0.5,
    Math.sin(satSpeed) * satOrbitRadius);

  objects[2].position.set(
    Math.cos(date) * moonOrbitRadius * -1, 
    Math.cos(date) * moonOrbitRadius * -1 -0.5, 
    //0,
    Math.sin(date) * moonOrbitRadius + 1);
    // end Stack Overflow Code for Orbiting
};


//console.log(objects);
animate();