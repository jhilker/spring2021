-----
title: Lab 3 Questions
author: Jacob Hilker
-----
1. Yes, the display changes. When I was testing this, I decided to `console.log` the modelViewMatrix so that I could see what it looked like. I found that it was saying it was undefined - specifically, an `Uncaught ReferenceError`.
2. Nothing is drawn to the screen. Again, I got the `Uncaught ReferenceError` with the modelViewMatrix.
4a. When the canvas is 512x256, it cuts the aspect ratio in half along the y axis. 
4b. When the canvas is 256x512, it cuts the aspect ratio in half along the x axis.
10. To look down the y axis, I rotated along the X-axis by 90 degrees. To look at the tops of the cubes without changing the x or y axes, you rotate along the Z axis by 180 degrees.