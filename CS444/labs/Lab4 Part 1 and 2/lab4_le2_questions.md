---
title: Lab 4 LE2 Questions
author: Jacob Hilker
---

2. The default light is a positional light - the last number in `lpos` is a 1.
3. The `mv` matrix had an effect when I had the `mv` inside the matrix, because it's being translated before positioning the light from where it was translated to.
4. I did not see a difference with either positional or directional.
5. The effect is so different because the amount of light is essentially doubling if you have both the original light and the second light in the same spot. However, if one light is directional and one light is positional, the effect is much more noticable because one is applied after the mv is applied to the light, while with directional it's coming directly from the xyz position of the light. 
6. See `diagram.png`.