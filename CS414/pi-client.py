#!/usr/bin/python3

import socket

# open the file and read our value of Pi
f = open("pi.txt", "r")
pi = f.read()

# analysis code here
#In order to measure the closeness of two sequences, we can use the Levenshtein distance, 
#which will tell us the number of changes that were made between the two sequences. 
#The algorithm is difficult to implement, so you can use this Python function to compute it.
def MED_character(str1,str2):
    cost = 0
    len1 = len(str1)
    len2 = len(str2)

    # output the length of other string in case the length of any of the string is zero
    if len1 == 0:
        return len2
    if len2 == 0:
        return len1

    # initializing a zero matrix
    accumulator = [[0 for x in range(len2)] for y in range(len1)] 

    # initializing the base cases
    for i in range(0, len1):
        accumulator[i][0] = i;
    for i in range(0, len2):
        accumulator[0][i] = i;

    # we take the accumulator and iterate through it row by row. 
    for i in range(1,len1):
        char1 = str1[i]
        for j in range(1,len2):
            char2 = str2[j]
            cost1 = 0
            if char1 != char2:
                cost1 = 2 # cost for substitution
            accumulator[i][j]=min(accumulator[i-1][j]+1, accumulator[i][j-1]+1, accumulator[i-1][j-1] + cost1 )

    cost = accumulator[len1 - 1][len2 - 1]
    return cost



# put in these values
HOST = '127.0.0.1'
PORT = 4040

# create our UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# send the request to the server using sendTo method on a particular HOST-IP address and PORT number 
print("Sending the request...")
sock.sendto(b"pi", ((HOST, PORT)))

print("Receiving Pi...", end="")

# keep track of our version of pi
received = ""

# receive the digits of pi one by one
while True:
    digit, address = sock.recvfrom(1)
    #print(address)
    digit = digit.decode()
    if digit == '-':
        break
    else:
        received = received + digit
        
print(len(received))
sock.close()
print(MED_character(received,pi))
print("Done!")

