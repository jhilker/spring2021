from socket import *
import datetime
server_port = 4040
server_socket = socket(AF_INET,SOCK_STREAM)
server_socket.bind(('',server_port))
server_socket.listen(1)
print("The server is ready to receive")
while 1:
     connection_socket, addr = server_socket.accept()
     sentence = connection_socket.recv(1024)
     x = datetime.datetime.now()
     connection_socket.send((str.encode(x.strftime("%A, %B %d, %Y, %I:%M %p\n"))))
     connection_socket.close()
